import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { RestApiProvider } from '../../providers/rest-api/rest-api';
import { CoreProvider } from '../../providers/core/core';



@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  signupData
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private builder: FormBuilder,
    private api: RestApiProvider,
    private core: CoreProvider,
    public event: Events,


  ) {
    this.signupData = this.builder.group({
      'first_name': ['', Validators.compose([Validators.required, Validators.maxLength(100), Validators.minLength(3)])],
      'last_name': ['', Validators.compose([Validators.required, Validators.maxLength(100), Validators.minLength(3)])],
      'username': ['', Validators.compose([Validators.required, Validators.maxLength(100), Validators.minLength(3)])],
      'email': ['', Validators.compose([Validators.required, Validators.maxLength(100), Validators.pattern('[a-zA-Z0-9._-]+@[a-zA-Z0-9_-]+\.[a-zA-Z]{2,4}')])],
      'password': ['', Validators.compose([Validators.required, Validators.maxLength(100), Validators.minLength(3)])],
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  //signup function
  signup() {
    this.core.showLoading('Logging...')
    let userData = {
      first_name: this.signupData.value.first_name,
      last_name: this.signupData.value.last_name,
      username: this.signupData.value.username,
      email: this.signupData.value.email.toLowerCase(),
      password: this.signupData.value.password,
    }
    console.log('userData : ', userData);

    this.api.signup(userData)
    .then(val => {
      this.core.hideLoading();
      console.log('val : ', val);
      if (val['status'] == 'false') {
        // TODO: handel err

      } else {
        this.event.publish('logged', val);
        this.navCtrl.setRoot('TracksPage');
      }

    })
    .catch(err => {
      this.core.hideLoading();
      console.log('err : ', err);
    })
  }

}
