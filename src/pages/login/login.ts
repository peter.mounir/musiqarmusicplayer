import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { RestApiProvider } from '../../providers/rest-api/rest-api';
import { CoreProvider } from '../../providers/core/core';



@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loginData: any;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private builder: FormBuilder,
    private api: RestApiProvider,
    private core: CoreProvider,
    public event: Events,

  ) {
    this.loginData = this.builder.group({
      'email': ['', Validators.compose([Validators.required, Validators.maxLength(100), Validators.pattern('[a-zA-Z0-9._-]+@[a-zA-Z0-9_-]+\.[a-zA-Z]{2,4}')])],
      'password': ['', Validators.compose([Validators.required, Validators.maxLength(100), Validators.minLength(3)])]
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  //login function
  Login() {
    this.core.showLoading('Logging...')
    let userData = {
      email: this.loginData.value.email.toLowerCase(),
      password: this.loginData.value.password
    }
    this.api.login(userData)
      .then(val => {
        this.core.hideLoading();
        console.log('val : ', val);
        if (val['status'] == 'false') {
          // TODO: handel err

        } else {
          this.event.publish('logged', val);
          this.navCtrl.setRoot('TracksPage');
        }

      })
      .catch(err => {
        this.core.hideLoading();
        console.log('err : ', err);
      })
  }
}
