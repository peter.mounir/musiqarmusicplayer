import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CoreProvider } from '../core/core';
import { Events } from 'ionic-angular';
import { environment } from '../../environment/environment';
import { Storage } from '@ionic/storage';


@Injectable()
export class RestApiProvider {

  access_token: string = null;
  refresh_token: string = null;
  token_type: string = null;

  _options: any
  apiUrl: string = environment.app_url;
  client_id: number = environment.client_id;
  client_secret: string = environment.client_secret;

  constructor(
    public http: HttpClient,
    public events: Events,
    private storage: Storage,
    private core: CoreProvider
  ) {
    this.events.subscribe('logged', data => {
      console.log('RestApiProvider logged data', data);
      this.access_token = data.access_token
      this.refresh_token = data.refresh_token
      this.access_token = data.token_type

      this.storage.set('userData', data)
    })
  }




  /* ######### Auth ######### */

  login(data) {
    let parameters = {
      'client_id': this.client_id,
      'client_secret': this.client_secret,
      'email': data.email,
      'password': data.password
    }
    this.getOptions();
    return new Promise((resolve, reject) => {
      this.http.post(`${this.apiUrl}auth/login`, parameters, this._options)
        .subscribe((data: any) => {
          resolve(data)
        }, err => {
          reject(err);
        });
    });
  }




  signup(data) {
    let parameters = {
      'client_id': this.client_id,
      'client_secret': this.client_secret,
      'first_name': data.first_name,
      'last_name': data.last_name,
      'username': data.username,
      'email': data.email,
      'password': data.password
    }
    this.getOptions();
    return new Promise((resolve, reject) => {
      this.http.post(`${this.apiUrl}auth/register`, parameters, this._options)
        .subscribe((data: any) => {
          resolve(data)
        }, err => {
          reject(err);
        });
    });
  }



  /* ######### Tracks Api  ######### */

  async getTracks() {
    await this.getOptions();
    return new Promise((resolve, reject) => {
      let hed = {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          "Authorization": `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImMwYzVjZmVmYWUyZTQxNzE1OWRjOTZiZTY5MmE0YzM2ZTliMmZlNjcwNTljMTQyNTcyMGYyMThkNjMzZmQxMWQ1MzYxNzMy`
        })
      }
      this.http.get(`${this.apiUrl}tracks/`, {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          "Authorization": `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImMwYzVjZmVmYWUyZTQxNzE1OWRjOTZiZTY5MmE0YzM2ZTliMmZlNjcwNTljMTQyNTcyMGYyMThkNjMzZmQxMWQ1MzYxNzMy`
        })
      })
        .subscribe((data: any) => {
          console.log('tracks data : ', data);
          resolve(data)
        }, err => {
          console.log('tracks err : ', err);
          reject(err);
        });
    });
  }



  async getTracksFM() {
    await this.getOptions();
    return new Promise((resolve, reject) => {

      this.http.get(`http://ws.audioscrobbler.com/2.0/?method=tag.gettoptracks&tag=disco&api_key=b3565c9d0d50c9428d3714b0887d2985&format=json`)
        .subscribe((data: any) => {
          console.log('tracks data : ', data);
          resolve(data)
        }, err => {
          console.log('tracks err : ', err);
          reject(err);
        });
    });
  }



  /* ######### Get Options ######### */

  async getOptions() {
    let data = await this.storage.get("userData")
    console.log('userData : ', data);
    if (data !== undefined) {
      if (data !== null) {
        this.access_token = data.access_token
        this.token_type = data.token_type

        this._options = {
          headers: new HttpHeaders({
            "Content-Type": "application/json",
            "Authorization": `Bearer ${this.access_token}`
          })
        }
      } else {
        this._options = {
          headers: new HttpHeaders({
            "Content-Type": "application/json",
          })
        }
      }
    } else {
      this._options = {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
        })
      }
    }
  }
}
