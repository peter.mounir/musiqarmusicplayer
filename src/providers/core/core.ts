import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';
import { ToastController, AlertController, LoadingController } from 'ionic-angular';


@Injectable()
export class CoreProvider {

  loading: any;
  isLoading: boolean;

  tosting: any;
  isTosting: boolean;

  constructor(
    private network: Network,
    private toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
  ) {
    console.log('Hello CorekProvider Provider');
  }


  /* ######### Check Network ######### */

  connectSubscription() {
    this.network.onConnect().subscribe(() => {
      console.log('network connected!');
      this.hideNetworkTost()
    });
  }



  disconnectSubscription() {
    this.network.onDisconnect().subscribe(() => {
      console.log('network was disconnected :-(');
      this.showNetworkTost()
    });
  }



  checkConnection() {
    setTimeout(() => {
      if (this.network.type == "none") {
        console.log('we got a wifi connection, woohoo!');
        this.showNetworkTost()
      }
    }, 3000);
  }



  /* ######### Messaging ######### */

  showLoading(content: string = null) {
    if (!this.isLoading) {
      this.isLoading = true;
      this.loading = this.loadingCtrl.create({
        content: content
      });
      this.loading.onDidDismiss(() => {
        this.isLoading = false
      });
      this.loading.present();
      setTimeout(() => { this.hideLoading() }, 30000);
    }
  }



  hideLoading() { if (this.isLoading) this.loading.dismiss(); }



  showNetworkTost() {
    if (!this.isTosting) {
      this.isTosting = true;

      this.tosting = this.toastCtrl.create({
        message: 'لا يوجد اتصال بالانترنت ...',
        position: 'top'
      });

      this.tosting.onDidDismiss(() => {
        this.isTosting = false
      });
      this.tosting.present();
      setTimeout(() => { this.hideNetworkTost() }, 30000);
    }
  }



  hideNetworkTost() { if (this.isTosting) this.tosting.dismiss(); }



  presentToast(mess) {
    let toast = this.toastCtrl.create({
      message: mess,
      duration: 2000,
      position: 'top'
    });
    toast.present();
  }


  showAlert(title, text) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: ['موافق']
    });
    alert.present();
  }
}
