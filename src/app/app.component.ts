import { Component } from '@angular/core';
import { Platform, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { RestApiProvider } from '../providers/rest-api/rest-api';
import { CoreProvider } from '../providers/core/core';
import { Storage } from '@ionic/storage';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  // rootPage: string = 'WelcomePage';
  rootPage: string = ""
  foundUser: boolean = false
  constructor(
    public events: Events,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private storage: Storage,
    public core: CoreProvider,
    private api: RestApiProvider,
  ) {
    this.isLoggedUser();

    

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  isLoggedUser() {
    this.storage.get("userData").then(async val => {
      if (val !== undefined) {
        if (val !== null) {
          let userData = val
          this.events.publish('logged', userData);
          this.foundUser = true;
          this.rootPage = 'TracksPage';
        } else {
          this.foundUser = false;
          this.rootPage = 'WelcomePage';
        }
      }
    }).catch(() => {
      this.foundUser = false;
      this.rootPage = 'WelcomePage';
    })
  }
}

