import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';

// providers
import { CoreProvider } from '../providers/core/core';
import { RestApiProvider } from '../providers/rest-api/rest-api';

// native
import { Network } from '@ionic-native/network';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { Facebook } from '@ionic-native/facebook';
import { Media } from '@ionic-native/media';
import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';
import { PreferencesService } from '../services/preferences.service';
import { MediaService } from '../services/media.service';
import { FavouritesService } from '../services/favourites.service';
import { CacheService } from '../services/cache.service';

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicStorageModule.forRoot({
      name: '__musiqardb'
    }),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Network,
    Facebook,
    Media,
    File,
    FileTransfer,
    CacheService,
    FavouritesService,
    MediaService,
    PreferencesService,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    CoreProvider,
    RestApiProvider
  ]
})
export class AppModule { }
